#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>
#include <string>
#include <signal.h>
#include <iostream>

#define MAX_MESSAGE_LEN 1557
using namespace std;
void sig_handler(int sig)
{
	if(sig == SIGUSR1)
	{
		pthread_exit(0);
		exit(0);
	}
}

void usage()
{
	printf("syntax : echo-client <ip> <port>\n");
	printf("sample : echo-client 192.168.10.2 1234\n");
}

void* send_thread(void* arg)
{	
	int handle = *(int*)arg;
	int send_size;
    while(1)
    {
    	string send_str;
    	getline(cin, send_str);
    	send_size = send(handle, send_str.c_str(), send_str.size(), 0);
    	if(send_size == 0)
		{
			printf("connection end to %d\n", handle);
			break;
		}
		if (send_size == -1)
		{
			printf("send error code -1\n");
			break;
		}
    }
    pthread_exit(0);
}

int main(int argc, char** argv)
{
	if(argc != 3)
	{
		usage();
		return 0;
	}

	int port = stoi(argv[2]);
	//printf("%d\n", port);
	//int port = 1234;
	int handle = socket(AF_INET, SOCK_STREAM, 0);

	if(signal(SIGUSR1, sig_handler) == SIG_ERR)
	{
        printf("signal handler error\n");
        return -1;
    }

	if(handle < 0)
	{
		printf("socket error\n");
		return -1;
	}
	int bf = 1;
	setsockopt(handle, SOL_SOCKET, SO_REUSEADDR, (char *)&bf, (int)sizeof(bf));

	struct sockaddr_in server_addr;
	memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(port);
    inet_pton(AF_INET, argv[1], &(server_addr.sin_addr));

    if (connect(handle, (struct sockaddr*)&server_addr, sizeof(server_addr)) < 0)
    {
		printf("connect error\n");
		return -1;
	}

    pthread_t thread_id;
	if (pthread_create(&thread_id, NULL, &send_thread, (void*)&handle) != 0)
    {
    	printf("pthread_create error");
    	return -1;
    }
    //pthread_detach(thread_id);
    
	char buf[MAX_MESSAGE_LEN];
	int recv_size;
	while(1)
	{
		recv_size = recv(handle, buf, sizeof(buf), 0);
		if(recv_size == 0)
		{
			printf("connection end %d\n", handle);
			break;
		}
		if (recv_size == -1)
		{
			printf("recv error code -1\n");
			break;
		}
		buf[recv_size] = '\0';
		printf("%s\n", buf);
	}
	printf("server end\n");

    pthread_kill(thread_id, SIGUSR1);
	close(handle);
	return 0;
}