#include <unistd.h>
#include <set>
#include <stdio.h>
#include <pthread.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>
#include <string>

#define MAX_CLIENTS 316
#define MAX_MESSAGE_LEN 1557
using namespace std;

pthread_mutex_t clients_lock = PTHREAD_MUTEX_INITIALIZER;

typedef struct asdf
{
	int id;
	struct sockaddr_in addr;
}Thread_args;

set <int> clients;
int echo = 0;
int broadcast = 0;

void usage()
{
	printf("syntax : echo-server <port> [-e[-b]]\n");
	printf("sample : echo-server 1234 -e -b\n");
}

void* listen_thread(void *arg)
{
	Thread_args* parse_arg = (Thread_args*)arg;
	int client_id = parse_arg -> id;
	struct sockaddr_in client_addr = parse_arg -> addr;
	char buf[MAX_MESSAGE_LEN];
	int recv_size;
	while(1)
	{
		recv_size = recv(client_id, buf, sizeof(buf), 0);
		if(recv_size == 0)
		{
			printf("connection end to %d\n", client_id);
			pthread_mutex_lock(&clients_lock);
			clients.erase(client_id);
			pthread_mutex_unlock(&clients_lock);
			break;
		}
		if (recv_size == -1)
		{
			printf("recv error code -1\n");
			pthread_mutex_lock(&clients_lock);
			clients.erase(client_id);
			pthread_mutex_unlock(&clients_lock);
			break;
		}
		buf[recv_size] = '\0';
		printf("%s\n", buf);
		if(echo == 1)
		{
			int send_ret = send(client_id, buf, recv_size, 0);
			if(send_ret == 0 || send_ret == -1)
			{
				printf("send error\n");
				pthread_mutex_lock(&clients_lock);
				clients.erase(client_id);
				pthread_mutex_unlock(&clients_lock);
				break;
			}
		}
		if(broadcast == 1)
		{
			set<int>::iterator i;
			int isbreak = 0;
			pthread_mutex_lock(&clients_lock);
			for(i = clients.begin(); i != clients.end(); i++)
			{
				int send_ret = send(*i, buf, recv_size, 0);
				if(send_ret == 0 || send_ret == -1)
				{
					printf("send error\n");
					clients.erase(*i);
					if(*i == client_id)
					{
						isbreak = 1;
					}
				}
			}
			pthread_mutex_unlock(&clients_lock);
			if(isbreak == 1)
			{
				break;
			}
		}
	}
	printf("thread exit\n");
	close(client_id);
	free(arg);
	pthread_exit(0);
}

int main(int argc, char** argv)
{
	if(argc < 2 || argc > 5)
	{
		usage();
		return 0;
	}
	for(int i = 2; i < argc; i++)
	{
		if(strcmp(argv[i], "-e") == 0)
		{
			echo = 1;
			continue;
		}
		else if(strcmp(argv[i], "-b") == 0)
		{
			broadcast = 1;
			continue;
		}
		else
		{
			printf("invalid option\n");
			usage();
			return 0;
		}
	}
	int port = stoi(argv[1]);
	//int port = 1234;
	int handle = socket(AF_INET, SOCK_STREAM, 0);
	if(handle < 0)
	{
		printf("socket error\n");
		return -1;
	}
	int bf = 1;
	setsockopt(handle, SOL_SOCKET, SO_REUSEADDR, (char *)&bf, (int)sizeof(bf));

	struct sockaddr_in server_addr;
	memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(port);

    if (bind(handle, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0)
    {
		printf("bind error\n");
		return -1;
	}
    if (listen(handle, MAX_CLIENTS) < 0)
    {
		printf("listen error\n");
		return -1;
	}

    socklen_t client_addr_len = sizeof(sockaddr_in);
    pthread_t thread_id;

    while(1)
    {
    	Thread_args* arg_ptr = (Thread_args*)malloc(sizeof(Thread_args));
    	arg_ptr -> id = accept(handle, (struct sockaddr*)&(arg_ptr -> addr), &client_addr_len);
    	pthread_mutex_lock(&clients_lock);
    	clients.insert(arg_ptr -> id);
    	pthread_mutex_unlock(&clients_lock);
    	if (pthread_create(&thread_id, NULL, &listen_thread, (void*)arg_ptr) != 0)
    	{
    		printf("pthread_create error");
    		return -1;
    	}
    	pthread_detach(thread_id);
    }
}